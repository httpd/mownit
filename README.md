# README #

Aplikacja prezentuje implementacje Sygnatur Wykonania.

### Funkcjonalność ###

Po uruchomieniu aplikacja wyświetla puste okno Signatures oraz okno sterujące z przyciskami "Run" i "Save Metric"
Naciśnięcie przycisku "Run" uruchamia program testowy "test" oraz okno z metryką. Dane są prezentowane w oknie w następujący sposób:

* Czerwony wykres to wykres zużycia CPU przez program testowy
* Niebieski wykres to wykres metryki stworzonej na podstawie danych CPU
* Czarne pionowe linie to markery programu "test", zapisane podczas pracy do pliku "test.txt"

Naciśnięcie przycisku "Save metric" zapisuje aktualnie wygenerowaną metrykę oraz jej markery jako metrykę porównawczą.

Ponowne naciśnięcie przycisku Run powoduje uruchomienie nowej instancji programu "test" oraz zatrzymanie poprzednio wykonywanej. Pojawia się nowe okno z wykresami odpowiadającymi nowej instancji programu. W oknie "Saved metrics" widoczne są wszystkie zapisane metryki oraz odpowiadające im parametry fitness określające podobieństwo do metryki aktualnie wykonywanej instancji programu testowego. Pod listą metryk widoczne jest podsumowanie zawierające numer metryki o najwyższym stopniu dopasowania.

### Uruchamianie ###

* make test
* sbt clean run Draw
import java.lang
import java.lang.Math
import java.util.Dictionary

/**
 * Created by piotrek on 17.05.16.
 */
class RuntimeMetric() {
  var startTime = System.currentTimeMillis
  var compressedMetrics = scala.collection.mutable.Map.empty[String, PolylineFit] // scala.collection.mutable.Map[PolylineFit, String] = Map()
  var markers = new Array[Long](0)
  def compareWithMetric(metric: RuntimeMetric) : Double  = {
//    return math.random

    var mark1 = markers map {x => x-startTime}
    var l1 = mark1.toBuffer

    var mark2 = metric.markers map {x => x-startTime}
    var l2 = mark1.toBuffer

    var m1 = compressedMetrics("CPU").arr.toBuffer.sortWith(_._3 < _._3)
    var m2 = metric.compressedMetrics("CPU").arr.toBuffer.sortWith(_._3 < _._3)

    var IntegDIF:Double = 0;
    var IntegMY:Double = 0;

    val lastMark1 = 0
    val lastMark2 = 0

    var lastC1:Long = 0
    var lastC2:Long = 0
    var scale = 1

    var calka1:Double = 0
    var calkaALL:Double = 0
    while(m1.nonEmpty &&  m2.nonEmpty){
//      val nextMark1 = l1.head
//      val nextMark2 = l2.head

//      val proporcje = (nextMark1 - lastMark1) / (nextMark2 - lastMark2)
      val obrabiany1 = m1.head
      val obrabiany2 = m2.head

//      val koniec1 = math.min(nextMark1, obrabiany1._3)
//      val koniec2 = math.min(nextMark1, obrabiany1._3)

      if (obrabiany1._3 > obrabiany2._3){ //konczymy w 2, (lastMark1, nextMark2)
        val calka = 0.5*(obrabiany1._1 - obrabiany2._1)*(obrabiany2._3 - lastC1)*(obrabiany2._3 - lastC1) +(obrabiany1._2 - obrabiany2._2)*(obrabiany2._3 - lastC1)
        lastC1 = obrabiany2._3
        calkaALL += 0.5*obrabiany1._1*(obrabiany2._3 - lastC1)*(obrabiany2._3 - lastC1) + obrabiany1._2*(obrabiany2._3 - lastC1)
        m2 = m2.drop(1)
        calka1 += calka


      }else {
        val calka = 0.5*(obrabiany1._1 - obrabiany2._1)*(obrabiany1._3 - lastC1)*(obrabiany1._3 - lastC1) +(obrabiany1._2 - obrabiany2._2)*(obrabiany1._3 - lastC1)
        calkaALL += 0.5*obrabiany1._1*(obrabiany2._3 - lastC1)*(obrabiany1._3 - lastC1) + obrabiany1._2*(obrabiany1._3 - lastC1)
        lastC1 = obrabiany1._3
        m1 = m1.drop(1)

      }



    }
    math.max(1-calka1/calkaALL, 0)
  }
}

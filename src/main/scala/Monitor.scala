/**
 * Created by piotrek on 09.05.16.
 */
import sys.process._

class Monitor(val pid: Int) {
  var time = System.currentTimeMillis
  def processExists = pid == {try { (List("ps", "-p", s"$pid", "-o", "pid=") !!).trim.toInt } catch { case _: Throwable => -1 }}
  def scan = {
    val  result = "ps -p " + pid.toString +" -o %cpu" !!;
    val lines = result.replace(',','.').split("\\n");
    (lines(1).toDouble, System.currentTimeMillis - time)
  }
}

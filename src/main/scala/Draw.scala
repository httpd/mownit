/**
 * Created by piotrek on 09.05.16.
 */

import java.io.{PrintWriter, ByteArrayOutputStream}
import javax.swing.DefaultListModel

import scala.collection.mutable.ListBuffer
import scala.swing
import scala.swing.event.ButtonClicked
import sys.process._

import java.awt.Color
import scala.io.Source
import java.awt.event.ActionListener
import sun.misc.Signal
import sun.misc.SignalHandler

import scala.collection.mutable
import scala.swing._

class DataPanel() extends Panel  {

  //  val PID = 47681;
  //  print("Curent pid: ")
  //  var monitor = new Monitor(PID)
  var i = 0
  //  var cashed = new mutable.Queue[(Double, Long)]()
  //  var signals = new mutable.Queue[Long]()
  //  val runtime = new RuntimeMetric()
  //
  var cpuTestMetric:PolylineFit  = null
  //  runtime.compressedMetrics("CPU") = cpuTestMetric
  var time = System.currentTimeMillis
  //markers
  //  var markers = new Array[Long](0)
  var runtime:RuntimeMetric = null
  var cashed:mutable.Queue[(Double, Long)] = null

  def startDrawing(runtime:RuntimeMetric, cashed: mutable.Queue[(Double, Long)]) {
    this.runtime = runtime
    this.cashed = cashed
    this.cpuTestMetric = this.runtime.compressedMetrics("CPU")
    time = System.currentTimeMillis
  }

  override def paintComponent(g: Graphics2D) {
    if (runtime == null || cashed == null) {return}
    g.setColor(Color.RED)

    var x = 0
    val copy = cpuTestMetric.arr.clone()
    val sigcpy = mutable.Queue(runtime.markers:_*)
    for (m <- cashed.toList ){
      g.setColor(Color.RED)

      g.fillRect(x,g.getClipBounds.height - m._1.toInt*300/100 ,1,m._1.toInt*300/100)
      val t = m._2

      while (copy.nonEmpty && copy.front._3 < t ){
        copy.dequeue()
      }
      while(sigcpy.nonEmpty && sigcpy.front - runtime.startTime < t){
        sigcpy.dequeue()
      }
      if (copy.nonEmpty) {
        g.setColor(new Color(0, 0, 255, 200))
        g.fillRect(x, g.getClipBounds.height - ((copy.front._1 + copy.front._2 * t)*300/100.0f).toInt, 1,  ((copy.front._1 + copy.front._2 * t)*300/100.0f).toInt)

      }
      if(sigcpy.nonEmpty && sigcpy.front - time < t+250)
      {
        g.setColor(Color.BLACK)
        g.fillRect(x, 0, 1, g.getClipBounds.height)

      }
      x+=1
    }
  }
  val timer=new javax.swing.Timer(250, Swing.ActionListener(e =>
  {
    this.repaint()
  })).start()



}

object Draw extends SimpleSwingApplication {
  var savedMetrics = mutable.ArrayBuffer.empty[RuntimeMetric]
  var workingMetric:RuntimeMetric = null
  var cashed:mutable.Queue[(Double, Long)] = null

  def runAndGenerateMetric(): Unit ={

    println("NEW")
//    "."+path!
//    val result  = Runtime.getRuntime.exec("ls")
//    val field = result.getClass.getDeclaredField("pid")
//    val pid = field.getInt(result)
//    println("Wynik: " + pid)
//     val result =
//    result
//   val result =  "./startScript.sh" !
//    println("Wynik: "+ (result);
//    val stderrStream = new ByteArrayOutputStream
//    val stdoutWriter = new PrintWriter(stdoutStream)
//    val stderrWriter = new PrintWriter(stderrStream)
//    val exitValue = "./startScript.sh".!!
    val results1 = Process(Seq("bash", "-c", "ps -ef | awk '$8==\"./test\" {print $2}'")).!!
    if (!results1.isEmpty) {
      for (pid <- results1.split("\\r?\\n").map(_.toInt))  {
        s"kill $pid" !
      }
    }

    val process = Process(Seq("bash","-c","./test >> /dev/null &"))
    process.run()

    //    process.
//    stdoutWriter.close()
//    stderrWriter.close()
    val results = Process(Seq("bash", "-c", "ps -ef | awk '$8==\"./test\" {print $2}'")).!!
    val PID =  results.split("\\r?\\n").map(_.toInt).sortWith(_>_)(0)

    println("Result: " + PID)
//    process.destroy
//
//    val PID = exitValue.filter(_ >= ' ').toInt
    val monitor = new Monitor(PID)
    sys addShutdownHook{
      s"kill $PID" !
    }

    cashed = new mutable.Queue[(Double, Long)]()
    val runtime = new RuntimeMetric()
    workingMetric = runtime

    val cpuTestMetric = new PolylineFit(0.01)
    runtime.compressedMetrics("CPU") = cpuTestMetric
    val timer=new javax.swing.Timer(250, null)
    timer.addActionListener(Swing.ActionListener(e =>
    {
      if (!monitor.processExists) {
        timer.stop()
        return
      }

      if (cashed.length > 600) {
        cashed.dequeue()
      }
      val scanned = monitor.scan
      cashed.enqueue(scanned)
      cpuTestMetric.add(scanned._1, scanned._2)
    }))
    timer.start()

    val markerTimer=new javax.swing.Timer(3000, Swing.ActionListener(e =>
    {
      runtime.markers = Array.empty[Long]
      runtime.markers = Source.fromFile("test.txt").getLines.toArray.map(_.toLong)
    }))

    markerTimer.start()
    val drawingPanel = new DataPanel() {
      preferredSize = new Dimension(600, 300)
    }
    drawingPanel.startDrawing(runtime, cashed)
    def drawingWindw = new MainFrame{
      contents = drawingPanel
      title = "Metric nr: " + savedMetrics.size.toString
    }
    drawingWindw.visible = true


//        contents += drawingPanel
  }


  val secondFrame = new Frame {

    title = "Saved metrics"
    val boxPanel = new BoxPanel(Orientation.Vertical) {
      preferredSize = new swing.Dimension(200, 100)
    }
    contents = boxPanel

    def refresh(): Unit ={
        val metrics = savedMetrics.toArray
        boxPanel.contents.clear()
        var best = -1
        var bestfit:Double = -1
        for ((obj, index) <- metrics.zipWithIndex) {
          val infoString: String = {
            if (workingMetric == null){
              s"Metric nr $index"
            }else{
              val compared_to = workingMetric.compareWithMetric(obj)
              if ( compared_to > bestfit) {
                best = index
                bestfit = compared_to
              }
              s"Metric nr $index fit $compared_to"

            }
          }
          val view = new Label() {
            text = infoString
            background = Color.BLUE

          }
          boxPanel.contents += view

        }
      if (metrics.length > 0) {
        val view = new Label() {
          text = s"Best metric $best fit $bestfit"
          background = Color.BLUE

        }
        boxPanel.contents += view

      }

      boxPanel.revalidate()
        boxPanel.repaint()

    }

    val timer=new javax.swing.Timer(250, Swing.ActionListener(e =>
    {
      this.refresh()
    })).start()

    pack()
    open()

  }
  secondFrame.visible = true

  def refreshSaved(): Unit ={
    secondFrame.refresh()
  }

//  secondFrame.visible = true



  def top = new MainFrame {


    contents = new BoxPanel(Orientation.Vertical) {
      val runningPanel = new BoxPanel(Orientation.Horizontal) {
        val button = new Button {
          text = "Run"

        }
        val button2 = new Button {
          text = "Save metric"
          //          enabled = false
        }

        contents += button
        contents += button2
        listenTo(button)
        listenTo(button2)
        reactions += {
          case ButtonClicked(b) =>
            if (b==button) { Draw.runAndGenerateMetric() }
            else if(b==button2) { if(workingMetric != null) { savedMetrics.append(workingMetric)}; refreshSaved(); }

        }
      }
      contents += runningPanel
    }

  }
}
import scala.collection.mutable

/**
 * Created by piotrek on 10.05.16.
 */
class PolylineFit(epsilon: Double) {
  var arr = new mutable.Queue[(Double, Double, Long)]()

    var N: Int = 0
    var sumT: Long = 0
    var sumM: Double = 0
    var sumsqM: Double = 0
    var sumsqT: Long = 0
    var sumTM: Double = 0

  def reset() = {
    N = 0
    sumT = 0
    sumM = 0
    sumsqM = 0
    sumsqT = 0
    sumTM = 0

  }
    def a = {
    (sumM * sumsqT - sumT * sumTM) / (N * sumsqT - sumT * sumT).toDouble
  }
    def b = {
    (N * sumTM - sumM * sumT) / (N * sumsqT - sumT * sumT).toDouble
  }
    def sigma = {
    (a*a*N + 2*a*b*sumT - 2*a*sumM - 2 * b * sumTM + b * b * sumsqT + sumsqM)/N.toDouble
  }
    def error = {
    N * math.sqrt(sigma) / sumM
  }
    var aprim: Double = 0
    var bprim: Double = 0

  def add(m: Double, t: Long) ={ //m - metryka t - czas
    N+=1
    sumT += t
    sumM += m
    sumsqT += t*t
    sumsqM += m*m
    sumTM += t*m

    if (N > 1) {
      if (error > epsilon) {
        arr.enqueue((aprim, bprim, t))
        reset()
      }
      aprim = a
      bprim = b

    }


  }

}
